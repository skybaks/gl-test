import logging
import time
import argparse

logger = logging.getLogger(__name__)

if __name__ == '__main__':
    start_time = time.time()
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true', help='enables verbose logging')
    args = parser.parse_args()

    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.INFO,
        format="[%(asctime)s.%(msecs)03d] %(levelname)s:\t%(message)s",
        datefmt='%H:%M:%S'
    )

    number = 0xcdcdcdcd
    for i in range(1, 1000):
        number = (number >> 4) * i
        logger.debug(f"The number is {number}")

    logger.info(f"Complete in: {'{:.1f}'.format((time.time() - start_time) * 1000.0)}ms")
